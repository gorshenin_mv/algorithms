#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"

#pragma comment(linker, "/STACK:50000000")


using namespace std;


struct Leaf
{
    int key;
    Leaf * left;
    Leaf * right;
    Leaf * parent;
    int size;

    Leaf(int _key, Leaf * _parent)
    {
        key = _key;
        parent = _parent;
        left = nullptr;
        right = nullptr;
        size = 0;
    }

    ~Leaf()
    {
        if (this->parent != nullptr)
        {
            if (this->parent->left == this)
                this->parent->left = nullptr;
            else
                this->parent->right = nullptr;
        }

        delete left;
        delete right;
    }
};


int CalcSize(Leaf * _root)
{
    _root->size++;

    if (_root == nullptr)
        return _root->size;
    if (_root->left != nullptr)
        _root->size += CalcSize(_root->left);

    if (_root->right != nullptr)
        _root->size += CalcSize(_root->right);

    return _root->size;
}


int DelSubTree(Leaf * _root, int _key)
{
    if (_root != nullptr)
    {
        if (_key < _root->key)
        {
           return DelSubTree(_root->left, _key);
        }
        else if (_key > _root->key)
        {
            return DelSubTree(_root->right, _key);
        }
        else
        {
            auto tmp = _root->size;

            Leaf * prnt = _root->parent;
            while (prnt != nullptr)
            {
               prnt->size -= tmp;

               prnt = prnt->parent;
            }

            delete _root;

            return tmp;
        }
    }
    else return 0;
}


void FillTree(Leaf * &_root, Leaf * _parent, int _line, vector<vector<int>> &_data)
{
    _root = new Leaf(_data[_line][0], _parent);

    if (_data[_line][1] != 0)
        FillTree(_root->left, _root, _data[_line][1] - 1, _data);

    if (_data[_line][2] != 0)
        FillTree(_root->right, _root, _data[_line][2] - 1, _data);
}


int main()
{
    openedu_in fin;
    int sizeDat = fin.next_i32(-1);
    Leaf * root = nullptr;

    vector<vector<int>> data(sizeDat, vector<int>(3));

    if (sizeDat > 0)
    {
        for (auto i = 0; i < sizeDat; i++)
        {
            for (auto j = 0; j < 3; j++)
            {
                data[i][j] = fin.next_i32(-1);
            }
        }

        FillTree(root, nullptr, 0, data);
    }

    root->size = CalcSize(root);

    int sizeKeys = fin.next_i32(-1);

    openedu_out fout;

    for (auto i = 0; i < sizeKeys; i++)
    {
        auto key = fin.next_i32(-1);

        sizeDat -= DelSubTree(root, key);
        fout.print_i32(sizeDat);
        fout.print_char('\n');
    }

    return 0;
}
