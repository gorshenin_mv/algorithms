#include <iostream>
#include <vector>
#include "openedu.hpp"

using namespace std;

struct SearchingParams
{
    int lMin;
    int rMin;
    int lMax;
    int rMax;
    int target;
};

int main()
{
    openedu_in fin;

    auto datSize = fin.next_i32(-1);
    vector<int> dat(datSize);

    for (auto i = 0; i < datSize; i++)
        dat[i] = fin.next_i32(-1);

    auto paramsSize = fin.next_i32(-1);
    vector<SearchingParams> params(paramsSize);

    for (auto i = 0; i < paramsSize; i++)
    {
        params[i].lMin = params[i].lMax = -1;
        params[i].rMin = params[i].rMax = datSize;
        params[i].target = fin.next_i32(-1);
    }


    while (true)
    {
        unsigned int notFindCounter = 0;

        for (auto i = 0; i < paramsSize; i++)
        {
            if (params[i].lMin < params[i].rMin - 1)
            {
                notFindCounter++;
                auto m = (params[i].lMin + params[i].rMin) / 2;
                if (dat[m] < params[i].target)
                    params[i].lMin = m;
                else
                    params[i].rMin = m;
            }

            if (params[i].lMax < params[i].rMax - 1)
            {
                notFindCounter++;
                auto m = (params[i].lMax + params[i].rMax) / 2;
                if (dat[m] <= params[i].target)
                    params[i].lMax = m;
                else
                    params[i].rMax = m;
            }
        }

        if (!notFindCounter)
            break;
    }


    openedu_out fout;
    for (auto i = 0; i < paramsSize; i++)
    {
        SearchingParams &sp = params[i];
        auto l = dat[sp.rMin] == sp.target ? sp.rMin + 1 : -1;
        auto r = l == -1 ? l : sp.lMax + 1;

        fout.print_i32(l);
        fout.print_char(' ');
        fout.print_i32(r);
        fout.print_char('\n');
    }

    return 0;
}
