#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"

#pragma comment(linker, "/STACK:50000000")

using namespace std;

struct Leaf
{
    int key;
    Leaf * left;
    Leaf * right;

    Leaf(int _key)
    {
        key = _key;
        left = nullptr;
        right = nullptr;
    }
};


void FillTree(Leaf * &_root, int _line, vector<vector<int>> &_data)
{
    _root = new Leaf(_data[_line][0]);

    if (_data[_line][1] != 0)
        FillTree(_root->left, _data[_line][1] - 1, _data);

    if (_data[_line][2] != 0)
        FillTree(_root->right, _data[_line][2] - 1, _data);
}


int CalcHeight(Leaf * _root)
{
    int h1 = 0, h2 = 0;

    if (_root == nullptr)
        return 0;
    if (_root->left != nullptr)
        h1 = CalcHeight(_root->left);
    if (_root->right != nullptr)
        h2 = CalcHeight(_root->right);

    return max(h1, h2) + 1;
}


int main()
{
    openedu_in fin;
    int size = fin.next_i32(-1);
    Leaf * root = nullptr;

    vector<vector<int>> data(size, vector<int>(3));

    if (size > 0)
    {
        for (auto i = 0; i < size; i++)
        {
            for (auto j = 0; j < 3; j++)
            {
                data[i][j] = fin.next_i32(-1);
            }
        }

        FillTree(root, 0, data);
    }


    openedu_out fout;
    fout.print_i32(CalcHeight(root));

    return 0;
}
