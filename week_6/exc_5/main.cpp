#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"

#pragma comment(linker, "/STACK:50000000")


using namespace std;


struct Leaf
{
    int key;
    Leaf * left;
    Leaf * right;
    Leaf * parent;
    int size;

    Leaf(int _key, Leaf * _parent)
    {
        key = _key;
        parent = _parent;
        left = nullptr;
        right = nullptr;
        size = 0;
    }

    ~Leaf()
    {
        if (this->parent != nullptr)
        {
            if (this->parent->left == this)
                this->parent->left = nullptr;
            else
                this->parent->right = nullptr;
        }

        delete left;
        delete right;
    }
};


void FillTree(Leaf * &_root, int _line, vector<vector<int>> &_data, Leaf * _parent = nullptr)
{
    _root = new Leaf(_data[_line][0], _parent);

    if (_data[_line][1] != 0)
        FillTree(_root->left, _data[_line][1] - 1, _data, _root);

    if (_data[_line][2] != 0)
        FillTree(_root->right, _data[_line][2] - 1, _data, _root);
}


bool CheckTree(Leaf * _root, int _min, int _max)
{
    if (_root == nullptr)
        return true;

    if (_root != nullptr &&
        _root->left != nullptr &&
        _root->left->key >= _root->key)
        return false;

    if (_root != nullptr &&
        _root->right != nullptr &&
        _root->right->key <= _root->key)
        return false;

    if (_root->key <= _min)
        return false;

    if (_max <= _root->key)
        return false;

        bool l = CheckTree(_root->left, _min, _root->key);
        bool b = CheckTree(_root->right, _root->key, _max);

        return l && b;

}


int main()
{    
    openedu_in fin;
    int sizeDat = fin.next_i32(-1);
    Leaf * root = nullptr;
    const int MIN = -1000000001;
    const int MAX = 1000000001;

    vector<vector<int>> data(sizeDat, vector<int>(3));

    if (sizeDat > 0)
    {
        for (auto i = 0; i < sizeDat; i++)
        {
            for (auto j = 0; j < 3; j++)
            {
                data[i][j] = fin.next_i32(-1);
            }
        }

        FillTree(root, 0, data);
    }

    bool result = CheckTree(root, MIN, MAX);

    openedu_out fout;
    result ? fout.println("YES") : fout.println("NO");

    return 0;
}
