#include <iostream>
#include <iomanip>
#include <fstream>

using namespace std;

int main()
{
    const double DELTA = 0.000000001;

    ifstream fin("input.txt");

    int size = 0;
    fin >> size;

    double h = 0.0;
    fin >> h;

    fin.close();

    double l, m, r;


    l = 0; r = h;


    m = (l + r) / 2;
    int leftCounter = 2;
    double cur = m;
    double prev = h;


    while (true)
    {
        double next = cur * 2 + 2 - prev;

        if (next < 0) // Меняю левую границу высоты второй лампочки и начинаю поиск лампочек занова
        {
            l = m;
            m = (l + r) / 2;
            leftCounter = 2;
            cur = m;
            prev = h;
        }

        else if (next > cur) // Меняю правую границу высоты второй лампочки и начинаю поиск лампочек занова
        {
            r = m;
            m = (l + r) / 2;
            leftCounter = 2;
            cur = m;
            prev = h;
        }
        else if (next < DELTA) // Значит высота второй лампочки подобрана верно
        {
            leftCounter++;
            prev = cur;
            cur = next;
            break;
        }
        else
        {
            leftCounter++;
            prev = cur;
            cur = next;
        }
    }


    while (leftCounter < size)
    {
        double next = cur * 2 + 2 - prev;
        leftCounter++;
        prev = cur;
        cur = next;
    }


    ofstream fout("output.txt");
    fout << setprecision(20);
    fout << cur << flush;
    fout.close();

    return 0;
}
