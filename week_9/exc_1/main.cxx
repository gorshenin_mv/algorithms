#include <iostream>
#include <cmath>
#include <cstring>
#include "openedu.hpp"
#include <vector>

using namespace std;

const int X = 53;


int GetMaxXM(size_t _size)
{
    int x = X;

    if (_size == 0)
        return 53;

    for (size_t i = 0; i < _size - 1; ++i)
        x = (x * X);

    return x;
}


int PolynHash(const char * _str, size_t _strSize)
{
    int h = 0;

    int s = GetMaxXM(_strSize);

    for (size_t i = 0; i < _strSize; ++i)
    {
        s = s / X;
        int part = s * _str[i];
        h += part;
    }

    return h;
}


int FindSubStr(const char * _txt, size_t _txtSize, const char * _str, size_t _strSize)
{
    auto hStr = PolynHash(_str, _strSize);
    int hTxt = 0;
    auto maxI = _txtSize - _strSize + 1;
    int maxXM = GetMaxXM(_strSize);
    auto subLen = _strSize;

    for (size_t txtIdx = 0; txtIdx < maxI; ++txtIdx)
    {
        if (txtIdx != 0)
            hTxt = hTxt * X - _txt[txtIdx - 1] * maxXM + _txt[txtIdx - 1 + subLen];
        else
            hTxt = PolynHash(_txt, subLen);

        if (hStr == hTxt)
        {
            auto equal = true;
            auto strLen = txtIdx + subLen;

            for (size_t txtIdx2 = txtIdx, strIdx = 0; txtIdx2 < strLen; ++txtIdx2, ++strIdx)
            {
                if (_txt[txtIdx2] != _str[strIdx])
                {
                    equal = false;
                    break;
                }
            }

            if (equal)
                return txtIdx;
        }
    }

    return -1;
}


int main()
{
    openedu_in fin;
    string s = fin.next();
    const char * sub = s.c_str();
    size_t subSize = strlen(sub);
    string t = fin.next();
    const char * text = t.c_str();
    size_t textSize = strlen(text);

    int counter = 0;
    vector<int> numOfSymb;
    int r = 0;

    r = FindSubStr(text, textSize, sub, subSize);
    while (r != -1)
    {
        ++counter;
        numOfSymb.push_back(r);

        r = FindSubStr(text + r + 1, textSize - r - 1, sub, subSize);

        if (r != -1)
            r = r + numOfSymb.back() + 1;
        else
            r = -1;
    }

    openedu_out fout;
    fout.print_i32(counter);
    fout.print_char('\n');

    for(size_t i = 0; i < numOfSymb.size(); ++i)
    {
        fout.print_i32(numOfSymb[i] + 1);
        fout.print_char(' ');
    }

    return 0;
}
