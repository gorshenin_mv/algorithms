#include <iostream>
#include <cmath>
#include <cstring>
#include "openedu.hpp"
#include <vector>

using namespace std;

const int M = 53;


int GetMaxXM(size_t _size)
{
    int x = M;

    if (_size == 0)
        return x;

    for (size_t i = 0; i < _size - 1; ++i)
        x = (x * M);

    return x;
}



int PolynHash(const char * _str, size_t _strSize)
{
    int h = 0;

    for (size_t i = 0; i < _strSize; ++i)
        h = M * h + _str[i];

    return h;
}


int FindSubStr(const char * _txt, size_t _txtSize, const char * _str, size_t _strSize)
{
    int hStr = PolynHash(_str, _strSize);
    int hTxt = 0;
    int maxI = _txtSize - _strSize + 1;
    int maxXM = GetMaxXM(_strSize);

    for (int txtIdx = 0; txtIdx < maxI; ++txtIdx)
    {
        if (txtIdx != 0)
            hTxt = hTxt * M - _txt[txtIdx - 1] * maxXM + _txt[txtIdx - 1 + _strSize];
        else
            hTxt = PolynHash(_txt, _strSize);

        if (hStr == hTxt)
        {
            int hStrTmp = hStr * M - _str[0] * maxXM;
            int hTxtTmp = hTxt * M - _txt[txtIdx] * maxXM;

            if (hStrTmp == hTxtTmp)
            {
//                auto equal = true;
//                auto strLen = txtIdx + _strSize - 1;

//                for (int txtIdx2 = strLen, strIdx = _strSize - 1; txtIdx2 >= txtIdx; --txtIdx2, --strIdx)
//                {
//                    if (_txt[txtIdx2] != _str[strIdx])
//                    {
//                        equal = false;
//                        break;
//                    }
//                }

//                if (equal)
                    return txtIdx;
            }

        }
    }

    return -1;
}


int main()
{
    openedu_in fin;

    string s = fin.next();
    string t = fin.next();

    const char * sub = s.c_str();
    const char * text = t.c_str();

    size_t subSize = strlen(sub);
    size_t textSize = strlen(text);

    int counter = 0;
    vector<int> numOfSymb;
    int r = 0;

    r = FindSubStr(text, textSize, sub, subSize);

    while (r != -1)
    {
        ++counter;
        numOfSymb.push_back(r);

        r = FindSubStr(text + r + 1, textSize - r - 1, sub, subSize);

        if (r != -1)
            r = r + numOfSymb.back() + 1;
        else
            r = -1;
    }

    openedu_out fout;
    fout.print_i32(counter);
    fout.print_char('\n');

    for(size_t i = 0; i < numOfSymb.size(); ++i)
    {
        fout.print_i32(numOfSymb[i] + 1);
        fout.print_char(' ');
    }

    return 0;
}
