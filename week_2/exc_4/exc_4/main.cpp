#include <fstream>
#include <iostream>


void Sort(int * _data,                         // Сортируемый массив
          int _l, int _r,                      // Границы диапазона массива
          unsigned int _a, unsigned int _b)    // Индексы начала и конца исследуемого участка
{
    if (_l < _r)
    {
        int l = _l;
        int r = _r;

        int v = _data[(l + r) / 2];

        while (l <= r)
        {
            while (_data[l] < v)
                l++;
            while (_data[r] > v)
                r--;

            if (l <= r)
            {
                std::swap(_data[l++], _data[r--]);
            }
        }

        if (_a <= r)
            Sort(_data, _l, r, _a, _b);
        if (_b >= l)
            Sort(_data, l, _r, _a, _b);
    }
}

int main()
{
    std::ifstream fin("input.txt");
    if (!fin.is_open())
        return EXIT_FAILURE;

    unsigned int size = 0; fin >> size;
    unsigned int k1 = 0; fin >> k1;
    unsigned int k2 = 0; fin >> k2;

    int a = 0; fin >> a;
    int b = 0; fin >> b;
    int c = 0; fin >> c;

    int * inDat = new int[size];

    fin >> inDat[0];
    fin >> inDat[1];
    fin.close();

    for (unsigned int i = 2; i < size; i++)
    {
        inDat[i] = a * inDat[i - 2] + b * inDat[i - 1] + c;
    }

    Sort(inDat, 0, size - 1, k1 - 1, k2 - 1);

    std::ofstream fout("output.txt");
    if (!fout.is_open())
        return EXIT_FAILURE;

    for (unsigned int i = k1 - 1; i < k2; i++)
    {
        fout << inDat[i] << ' ';
    }

    fout << std::flush;
    fout.close();

    return 0;
}
