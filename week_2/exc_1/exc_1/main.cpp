#include <iostream>
#include <fstream>


template <typename T>
void Merge(T * _a, unsigned int _l_idx, unsigned int _r_idx, unsigned int _m_idx,
            T * _c)
{
    unsigned int m = _m_idx;
    unsigned int c_idx = 0;

    while (_l_idx < m || _m_idx <= _r_idx)
    {
        if ((_m_idx > _r_idx) || (_l_idx < m && _a[_l_idx] <= _a[_m_idx]))
        {
            _c[c_idx++] = _a[_l_idx++];
        }
        else
        {
            _c[c_idx++] = _a[_m_idx++];
        }
    }
}


template <typename T>
void Sort(T * _a, unsigned int _l_idx, unsigned int _r_idx, std::ofstream &_fout)
{
    if (_l_idx == _r_idx)
        return;
    else if (_r_idx - _l_idx == 1)
    {
        if (_a[_l_idx] > _a[_r_idx])
            std::swap(_a[_l_idx], _a[_r_idx]);

        _fout << _l_idx + 1 << ' ' << _r_idx + 1 << ' '
             << _a[_l_idx] << ' ' << _a[_r_idx] << '\n';
        return;
    }

    Sort(_a, _l_idx, (_l_idx + _r_idx) / 2, _fout);
    Sort(_a, (_l_idx + _r_idx) / 2 + 1, _r_idx, _fout);

    unsigned int buffer_size = _r_idx - _l_idx + 1;
    T * buffer = new T[buffer_size];

    Merge(_a, _l_idx, _r_idx, (_l_idx + _r_idx) / 2 + 1, buffer);

    for (unsigned int i = 0, j = _l_idx; i < buffer_size; i++, j++)
    {
        _a[j] = buffer[i];
    }

    _fout << _l_idx + 1 << ' ' << _r_idx + 1 << ' '
          << buffer[0] << ' ' << buffer[buffer_size - 1] << '\n';

    delete[] buffer;
}


int main()
{
    std::ifstream fin("input.txt");
    if (!fin.is_open())
        return EXIT_FAILURE;

    unsigned int size = 0;
    fin >> size;

    int * inData = new int[size];
    for (unsigned int i = 0; i < size; i++)
    {
        fin >> inData[i];
    }

    fin.close();

    std::ofstream fout("output.txt");
    if (!fout.is_open())
        return EXIT_FAILURE;

    Sort(inData, 0, size - 1, fout);

    for (unsigned int i = 0; i < size; i++)
    {
        fout << inData[i] << ' ';
    }
    fout << std::flush;

    fout.close();

    return 0;
}
