#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

inline bool WriteResult(bool _result)
{
    std::ofstream fout("output.txt");
    if (!fout.is_open())
        return false;

    switch (_result) {
    case true:
        fout << "YES" << std::endl;
        break;
    case false:
        fout << "NO" << std::endl;
        break;
    }

    fout.close();
    return true;
}

int main()
{
    std::ifstream fin("input.txt");
    if (!fin.is_open())
        return EXIT_FAILURE;

    unsigned int size = 0; fin >> size;
    unsigned int numOfBrackets = 0; fin >> numOfBrackets;

    if (numOfBrackets == 1)
    {
        if (WriteResult(true))
            return EXIT_SUCCESS;
        else return EXIT_FAILURE;
    }

    int* inData = new int[size];
    for (int i = 0; i < size; ++i)
    {
        fin >> inData[i];
    }
    fin.close();

    std::vector<int>* brackets = new std::vector<int>[numOfBrackets];

    for (int i = 0; i < numOfBrackets; i++)
    {
        for (int j = i; j < size; j += numOfBrackets)
        {
            brackets[i].insert(brackets[i].end(), inData[j]);
        }
    }

    for (int i = 0; i < numOfBrackets; i++)
    {
        std::sort(brackets[i].begin(), brackets[i].end());
    }

    for (int i = 0; i < numOfBrackets; i++)
    {
        for (int j = i, b_idx = 0; j < size; j += numOfBrackets, b_idx++)
        {
            inData[j] = brackets[i].at(b_idx);
        }
    }

    for (int i = 1; i < size; ++i)
    {
        if (inData[i - 1] > inData[i])
        {
            if (WriteResult(false))
                return EXIT_SUCCESS;
            else return EXIT_FAILURE;
        }
    }

    if (WriteResult(true))
        return EXIT_SUCCESS;
    else return EXIT_FAILURE;
}
