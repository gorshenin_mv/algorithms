#include <fstream>



int main()
{
    std::ifstream fin("input.txt");

    if (!fin.is_open())
        return EXIT_FAILURE;

    unsigned int size = 0;

    fin >> size;

    fin.close();

    unsigned int * array = new unsigned int[size];

    for (unsigned int i = 0; i < size; i++)
    {
        array[i] = i + 1;
    }

    for (unsigned int i = 2; i < size; i++)
    {
        if (i != i / 2)
            std::swap(array[i], array[i / 2]);
    }


    std::ofstream fout("output.txt");

    if (!fout.is_open())
        return EXIT_FAILURE;


    for (unsigned int i = 0; i < size; i++)
    {
        fout << array[i] << ' ';
    }

    fout << std::flush;

    fout.close();

    return 0;
}
