#include <iostream>
#include <list>
#include <cstring>
#include <fstream>
#include <queue>
#include <algorithm>
#include <string>
#include <unordered_map>

struct ProgramStr
{

};

struct HashTableElem
{
    std::string str;
    unsigned int idx;

    bool operator <(const HashTableElem& _elem)
    {
        return (*this).str < _elem.str;
    }

    bool operator <(const std::string& _str)
    {
        return (*this).str < _str;
    }

    bool operator ==(const HashTableElem& _elem)
    {
        return (*this).str == _elem.str;
    }

    bool operator ==(const std::string& _str)
    {
        //std::string tmp = (*this).str;

        return (*this).str == _str;
    }
};


// Возвращает количество строк в программе, если 0 - ошибка чтения файла
unsigned int ReadData(std::string* _program, std::unordered_map<std::string, int>* _hashtable)
{
    std::ifstream fin("input.txt");
    if (!fin.is_open())
        return 0;

    int program_idx = 0;

    while (fin)
    {
        std::getline(fin, _program[program_idx]);

        //std::string tmp = _program[program_idx];

        if (_program[program_idx][0] == ':')
        {
            std::string label(_program[program_idx], 1);

            _hashtable->operator [](label) = program_idx;

        }

        program_idx++;
    }

    fin.close();
    return program_idx - 1;
}

int ConvertToRegisterNumber(char _name)
{
    return static_cast<int>(_name) - 97;
}

int GetEntryPoint(std::string& _label, std::unordered_map<std::string, int>* _hashtable)
{
    return _hashtable->operator [](_label);

}

int main()
{    
    const unsigned int MAX_PROGRAM_SIZE = 100000;

    std::string* program = new std::string[MAX_PROGRAM_SIZE];
    std::unordered_map<std::string, int>* hashtable = new std::unordered_map<std::string, int>;

    int programSize = ReadData(program, hashtable);

    if (programSize == 0)
        return EXIT_FAILURE;

    std::queue<int> q;
    std::vector<int> registers(26);

    std::ofstream fout("output.txt");
    if (!fout.is_open())
        return EXIT_FAILURE;


    for (int programIdx = 0; programIdx < programSize; programIdx++)
    {
        switch (program[programIdx][0])
        {
        case '+':
        {
            int a = q.front(); q.pop();
            int b = q.front(); q.pop();
            q.push((a + b) & 65535);
            break;
        }
        case '-':
        {
            int a = q.front(); q.pop();
            int b = q.front(); q.pop();
            q.push((a - b) & 65535);
            break;
        }
        case '*':
        {
            int a = q.front(); q.pop();
            int b = q.front(); q.pop();
            q.push((a * b) & 65535);
            break;
        }
        case '%':
        {
            int div = q.front(); q.pop();
            int denom = q.front(); q.pop();
            denom ? q.push((div % denom) & 65535) : q.push(0);
            break;
        }
        case '/':
        {
            int div = q.front(); q.pop();
            int denom = q.front(); q.pop();
            denom ? q.push(div / denom) : q.push(0);
            break;
        }
        case '>':
        {
            int num = ConvertToRegisterNumber(program[programIdx][1]);
            registers[num] = q.front(); q.pop();
            break;
        }
        case '<':
        {
            int num = ConvertToRegisterNumber(program[programIdx][1]);
            q.push(registers[num]);
            break;
        }
        case 'P':
        {
            int val = 0;

            if (program[programIdx].size() == 1)
            {
                val = q.front(); q.pop();
            }
            else
            {
                int num = ConvertToRegisterNumber(program[programIdx][1]);
                val = registers[num];
            }

            fout << val << '\n';
            break;
        }
        case 'C':
        {
            int val = 0;

            if (program[programIdx].size() == 1)
            {
                val = q.front() & 255; q.pop();
            }
            else
            {
                int num = ConvertToRegisterNumber(program[programIdx][1]);
                val = registers[num] & 255;
            }

            fout << (char)val;
            break;
        }
        case ':':
        {
            break;
        }
        case 'J':
        {
            std::string label(program[programIdx], 1);
            programIdx = GetEntryPoint(label, hashtable);
            break;
        }
        case 'Z':
        {
            int num = ConvertToRegisterNumber(program[programIdx][1]);

            if (registers[num] == 0)
            {
                std::string label(program[programIdx], 2);
                programIdx = GetEntryPoint(label, hashtable);
            }

            break;
        }
        case 'E':
        {
            int num1 = ConvertToRegisterNumber(program[programIdx][1]);
            int num2 = ConvertToRegisterNumber(program[programIdx][2]);

            if (registers[num1] == registers[num2])
            {
                std::string label(program[programIdx], 3);
                programIdx = GetEntryPoint(label, hashtable);
            }

            break;
        }
        case 'G':
        {
            int num1 = ConvertToRegisterNumber(program[programIdx][1]);
            int num2 = ConvertToRegisterNumber(program[programIdx][2]);

            if (registers[num1] > registers[num2])
            {
                std::string label(program[programIdx], 3);
                programIdx = GetEntryPoint(label, hashtable);
            }

            break;
        }
        case 'Q':
        {
            programIdx = programSize + 1;
            break;
        }
        default:
        {
            if(program[programIdx].size() != 0)
            {
                int val = std::stoi(program[programIdx]);
                q.push(val);
            }

            break;
        }

        }
    }

    fout.close();

    return 0;
}
