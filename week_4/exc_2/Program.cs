﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Queue
{
    class Program
    {
        static int head = -1;
        static int tail = -1;
        static bool isEmpty=true;

        static void Enqueue(int[] _queue, int _val)
        {
            if (tail >= _queue.Length-1 && head != 0)
            {
                for (int newIdx = 0, idx = head; tail < _queue.Length; newIdx++, idx++)
                {
                    _queue[newIdx] = _queue[idx];
                }
            }
            else
            {
                if (isEmpty)
                {
                    head++;
                    tail++;
                    _queue[head] = _val;
                    isEmpty = false;
                }
                else
                {
                    tail++;
                    _queue[tail] = _val;
                }
            }
        }

        static int Dequeue(int[] queue)
        {
            int result = 0;

            if (!isEmpty)
            {
               if (head < tail)
            {
                result = queue[head];
                head++;
            }
            else if (head==tail)
            {
                result = queue[head];
                head = -1;
                tail = -1;
                isEmpty = true;
            } 
            }
            
            return result;
        }

        static void Main(string[] args)
        {
            int[] queue = new int[1000000];
            string line;
            string[] lineData;
            StringBuilder result = new StringBuilder();

            using (var reader = new StreamReader(new FileStream("input.txt", FileMode.Open)))
            {
                reader.ReadLine();

                while ((line = reader.ReadLine()) != null)
                {
                    lineData = line.Split(' ');

                    if (lineData[0] == "+")
                    {
                        Enqueue(queue, Convert.ToInt32(lineData[1]));
                    }
                    else
                    {
                        result.Append(Dequeue(queue));
                        result.Append('\n');
                    }
                }

            }

            using (var writer = new StreamWriter(new FileStream("output.txt", FileMode.Create)))
            {
                writer.Write(result);
            }
        }
    }
}
