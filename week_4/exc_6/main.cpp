#include <iostream>
#include <fstream>
#include <stack>
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>

using namespace std;

inline bool space(char c){
    return std::isspace(c);
}


inline bool notspace(char c){
    return !std::isspace(c);
}


//break a sentence into words
std::vector<std::string>* split(const std::string& s){
    typedef std::string::const_iterator iter;
    auto ret = new std::vector<std::string>;
    iter i = s.begin();
    while(i!=s.end()){
        i = std::find_if(i, s.end(), notspace); // find the beginning of a word
        iter j= std::find_if(i, s.end(), space); // find the end of the same word
        if(i!=s.end()){
            ret->push_back(std::string(i, j)); //insert the word into vector
            i = j; // repeat 1,2,3 on the rest of the line.
        }
    }
    return ret;
}

int main()
{
    auto s = new stack<int>;
    std::string data;

    std::fstream fin("input.txt");
    if (!fin.is_open())
        return EXIT_FAILURE;

    getline(fin, data);
    getline(fin, data);

    fin.close();

    std::vector<std::string>* v = split(data);

    for (unsigned int i = 0; i < v->size(); i++)
    {
        if (v->operator [](i).size() != 1)
        {
            int val = std::stoi(v->operator [](i));
            s->push(val);
        }
        else
        {
            if (v->operator [](i) == "+")
            {
                int first = s->top(); s->pop();
                int second = s->top(); s->pop();

                s->push(second + first);
            }
            else if (v->operator [](i) == "-")
            {
                int first = s->top(); s->pop();
                int second = s->top(); s->pop();

                s->push(second - first);
            }
            else if (v->operator [](i) == "*")
            {
                int first = s->top(); s->pop();
                int second = s->top(); s->pop();

                s->push(second * first);
            }
            else if (v->operator [](i) == "/")
            {
                int first = s->top(); s->pop();
                int second = s->top(); s->pop();

                s->push(second / first);
            }
            else
            {
                int val = std::stoi(v->operator [](i));
                s->push(val);
            }
        }
    }


    std::ofstream fout("output.txt");
    if (!fout.is_open())
        return EXIT_FAILURE;

    int result = s->top();
    fout << result << std::flush;
    fout.close();

    return 0;
}
