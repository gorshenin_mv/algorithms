﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stack
{
    class Program
    {
        static void Main(string[] args)
        {
            int quantity;
            string line;
            int[] stack;
            string[] lineData;
            int top = 0;
            StringBuilder result = new StringBuilder();

            using (var reader = new StreamReader(new FileStream("input.txt", FileMode.Open)))
            {
                quantity = Convert.ToInt32(reader.ReadLine());
                stack = new int[quantity];

                while ((line=reader.ReadLine())!=null)
                {
                    lineData = line.Split(' ');

                    if (lineData[0] == "+")
                    {
                        stack[top] = Convert.ToInt32(lineData[1]);
                        top++;
                    }
                    else
                    {
                        top--;
                        result.Append(stack[top]);
                        result.Append('\n');
                        //Console.WriteLine(stack[top]);
                    }
                }
            }

            //Console.ReadLine();
            using (var writer = new StreamWriter(new FileStream("output.txt", FileMode.Create)))
            {
                writer.Write(result.ToString());
            }
        }
    }
}
