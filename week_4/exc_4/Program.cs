﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace quemin
{
    class Program
    {


        struct StackItem
        {
            public Int32 min;
            public Int32 val;

            public StackItem(Int32 _min, Int32 _val)
            {
                min = _min;
                val = _val;
            }
        }


        static void SaveResult(StringBuilder _result, int _value)
        {
            _result.AppendLine(_value.ToString());
        }

        static void WriteResult(StringBuilder _result)
        {
            using (var writer = new StreamWriter(new FileStream("output.txt",FileMode.Create)))
            {
                writer.Write(_result);
            }
        }

        static void Main(string[] args)
        {
            string line;
            string[] data;           
            StringBuilder result = new StringBuilder();
            var stack1 = new Stack<StackItem>();
            var stack2 = new Stack<StackItem>();

            using (var reader = new StreamReader(new FileStream("input.txt", FileMode.Open)))
            {
                reader.ReadLine();

                while ((line = reader.ReadLine())!=null)
                {
                    data = line.Split(' ').ToArray();

                    if (data[0] == "+")
                    {
                        Int32 value = Convert.ToInt32(data[1]);

                        if (stack1.Count == 0)
                        {
                            stack1.Push(new StackItem(value, value));
                        }
                        else
                        {
                            if (value <= stack1.Peek().min)
                            {
                                stack1.Push(new StackItem(value, value));
                            }
                            else
                            {
                                stack1.Push(new StackItem(stack1.Peek().min, value));
                            }
                        }
                    }

                    else if (data[0] == "-")
                    {
                        if (stack2.Count == 0)
                        {
                            Int32 quantity = stack1.Count;

                            for (int i = 0; i < quantity; i++)
                            {
                                Int32 value = stack1.Pop().val;

                                if (stack2.Count == 0)
                                {
                                    stack2.Push(new StackItem(value, value));
                                }
                                else
                                {
                                    if (value <= stack2.Peek().min)
                                    {
                                        stack2.Push(new StackItem(value, value));
                                    }
                                    else
                                    {
                                        stack2.Push(new StackItem(stack2.Peek().min, value));
                                    }
                                }
                            }
                        }
                        stack2.Pop();
                    }
                    else if (data[0] == "?")
                    {
                        if (stack1.Count != 0 && stack2.Count != 0)
                        {
                            if (stack1.Peek().min <= stack2.Peek().min)
                            {
                                SaveResult(result, stack1.Peek().min);
                            }
                            else
                            {
                                SaveResult(result, stack2.Peek().min);
                            }
                        }
                        else if (stack1.Count == 0)
                        {
                            SaveResult(result, stack2.Peek().min);
                        }
                        else if (stack2.Count == 0)
                        {
                            SaveResult(result, stack1.Peek().min);
                        }
                    }
                }                
            }
            WriteResult(result);
        }
    }
}
