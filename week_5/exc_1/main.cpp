#include <string>
#include <fstream>
#include <iostream>
#include "openedu.hpp"

using namespace std;

int main()
{
//    ifstream fin("input.txt");
//    if (!fin.is_open())
//        return EXIT_FAILURE;

    int size = 0;
    auto isHeap = true;

    //fin >> size;
    openedu_in fin;
    size = fin.next_i32(-1);
    size += 1;

    int* inDat = new int[size];
    *inDat = -1;

    for (auto i = 1; i < size; i++)
    {
        inDat[i] = fin.next_i32(-1);
        //fin >> inDat[i];
        //cout << inDat[i] << ' ';
    }

    //fin.close();

    for (auto i = 1; i < size; i++)
    {
        if (2 * i < size && inDat[i] > inDat[2 * i])
        {
            isHeap = false;
            break;
        }

        if (2 * i + 1 < size && inDat[i] > inDat[(2 * i) + 1])
        {
            isHeap = false;
            break;
        }
    }

    string result = isHeap ? "YES" : "NO";

//    ofstream fout("output.txt");
//    if (!fout.is_open())
//        return EXIT_FAILURE;

//    fout << result << flush;
//    fout.close();

    openedu_out() << result;

    return EXIT_SUCCESS;
}
