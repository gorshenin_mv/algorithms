#include <iostream>
#include <vector>
#include <string>
#include <ctype.h>
#include "openedu.hpp"

using namespace std;

struct Node
{
    int key;
    int strIdx;

    bool operator <(Node _n)
    {
        return this->key < _n.key;
    }

    bool operator >(Node _n)
    {
        return this->key > _n.key;
    }
};


std::vector<std::string> split(const std::string &text, char sep) {
    std::vector<std::string> tokens;
    std::size_t start = 0, end = 0;
    while ((end = text.find(sep, start)) != std::string::npos) {
        if (end != start) {
          tokens.push_back(text.substr(start, end - start));
        }
        start = end + 1;
    }
    if (end != start) {
       tokens.push_back(text.substr(start));
    }
    return tokens;
}


void MinHeapify(vector<Node> &_arr, int _idx, int _lastIdx, vector<int> &_strNumToIdx)
{
    auto smallestIdx = _idx;

    if (_idx * 2 <= _lastIdx && _arr[smallestIdx] > _arr[_idx * 2])
        smallestIdx = _idx * 2;

    if (_idx * 2 + 1 <= _lastIdx && _arr[smallestIdx] > _arr[_idx * 2 + 1])
        smallestIdx = _idx * 2 + 1;

    if (smallestIdx != _idx)
    {
        swap(_strNumToIdx[_arr[_idx].strIdx], _strNumToIdx[_arr[smallestIdx].strIdx]);
        swap(_arr[_idx], _arr[smallestIdx]);
        MinHeapify(_arr, smallestIdx, _lastIdx, _strNumToIdx);
    }
}


void Insert(vector<Node> &_arr, Node _val, int &_lastIdx, vector<int> &_strNumToIdx)
{
    _lastIdx ++;
    _arr[_lastIdx] = _val;
    _strNumToIdx[_arr[_lastIdx].strIdx] = _lastIdx;

    auto parent = _lastIdx / 2;
    auto newNode = _lastIdx;

    while (parent >= 1 && _arr[parent] > _arr[newNode])
    {        
        swap(_strNumToIdx[_arr[parent].strIdx], _strNumToIdx[_arr[newNode].strIdx]);
        swap(_arr[parent], _arr[newNode]);

        newNode = parent;
        parent /= 2;
    }
}


bool ExtractMin(vector<Node> &_arr, int &_lastIdx, int * _min, vector<int> &_strNumToIdx)
{
    if (_lastIdx < 1)
    {
        return false;
    }

     *_min = _arr[1].key;

    _arr[1] = _arr[_lastIdx];
    _strNumToIdx[_arr[1].strIdx] = 1;

    _lastIdx --;

    MinHeapify(_arr, 1, _lastIdx, _strNumToIdx);

    return true;
}


void Decrease(vector<Node> &_arr, int _strNum, int _val, vector<int> &_strNumToIdx)
{
    auto idx = _strNumToIdx[_strNum];

    _arr[idx].key = _val;

    auto parent = idx / 2;
    auto newNode = idx;

    while (parent >= 1 && _arr[parent] > _arr[newNode])
    {
        swap(_strNumToIdx[_arr[parent].strIdx], _strNumToIdx[_arr[newNode].strIdx]);
        swap(_arr[parent], _arr[newNode]);

        newNode = parent;
        parent /= 2;
    }
}


int main()
{
    openedu_in fin;
    openedu_out fout;

    int size = 0;
    size = fin.next_i32(-1) + 1;
    vector<Node> inDat(size);
    vector<int> strNumToIdx(size);

    int lastIdx = 0;

    for (auto i = 1; i < size; i++)
    {
        string command(fin.next());

            if (command == "A")
            {
                Node elem { fin.next_i32(-1), i };

                Insert(inDat, elem, lastIdx, strNumToIdx);
            }
            else if (command == "X")
            {
                int min = 0;
                ExtractMin(inDat, lastIdx, &min, strNumToIdx) ? fout.print_i32(min) : fout.print_char('*');
                fout.print_char('\n');
            }
            else if (command == "D")
            {
                auto strNum = fin.next_i32(-1);
                auto newVal = fin.next_i32(-1);

                Decrease(inDat, strNum, newVal, strNumToIdx);
            }
    }

    return 0;
}
