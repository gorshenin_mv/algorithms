#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"
#include <stack>

#pragma comment(linker, "/STACK:100000000")

using namespace std;

struct Node
{
    int key;
    Node * left;
    Node * right;
    Node * parent;
    int balance;
    int height;

    Node(int _key, Node * _parent)
    {
        key = _key;
        parent = _parent;
        left = nullptr;
        right = nullptr;
        balance = 0;
        height = 0;
    }

    ~Node()
    {
        if (this->parent != nullptr)
        {
            if (this->parent->left == this)
                this->parent->left = nullptr;
            else
                this->parent->right = nullptr;
        }

        delete left;
        delete right;
    }
};

void FillTree(Node * &_root, int _line, vector<vector<int>> &_data, Node * _parent = nullptr)
{
    _root = new Node(_data[_line][0], _parent);

    if (_data[_line][1] != 0)
        FillTree(_root->left, _data[_line][1] - 1, _data, _root);

    if (_data[_line][2] != 0)
        FillTree(_root->right, _data[_line][2] - 1, _data, _root);
}


void CalcBalance(Node * _root)
{
    if (_root == nullptr)
    {
        return;
    }


    if (_root->left != nullptr)
        CalcBalance(_root->left);
    if (_root->right != nullptr)
        CalcBalance(_root->right);

    auto hl = _root->left == nullptr ? -1 : _root->left->height - 1;
    auto hr = _root->right == nullptr ? -1 : _root->right->height - 1;

    _root->balance = hr - hl;

    hl = hl == -1 ? 0 : ++hl;
    hr = hr == -1 ? 0 : ++hr;

    _root->height = max(hl, hr) + 1;
}


Node * Find(Node *_root, int _key)
{
    if (_root == nullptr)
        return nullptr;

    if (_root->key > _key)
        return Find(_root->left, _key);
    if (_root->key < _key)
        return Find(_root->right, _key);

    return _root;
}



void WrRes(openedu_out &_fout, Node * _root, vector<vector<int>> &_data, int _idx = 0)
{
    auto idx = _idx++;

    if (_root == nullptr)
        return;

    if (_root != nullptr)
    {
        _fout.print_i32(_root->balance);
        _fout.print_char('\n');
    }

    if (_idx >= _data.size())
        return;

    if (_data[_idx][1] <= _data[_idx][2])
    {
        if (_root->left != nullptr)
            WrRes(_fout, _root->left, _data, idx);

        if (_root->right != nullptr)
            WrRes(_fout, _root->right, _data, idx);
    }
    else
    {
        if (_root->right != nullptr)
            WrRes(_fout, _root->right, _data, idx);

        if (_root->left != nullptr)
            WrRes(_fout, _root->left, _data, idx);
    }
}




int main()
{
    openedu_in fin;
    int size = fin.next_i32(-1);
    Node * root = nullptr;

    vector<vector<int>> data(size, vector<int>(3));

    if (size > 0)
    {
        for (auto i = 0; i < size; i++)
        {
            for (auto j = 0; j < 3; j++)
            {
                data[i][j] = fin.next_i32(-1);
            }
        }

        FillTree(root, 0, data);
    }

    CalcBalance(root);

    openedu_out fout;
    Node ** nodes = new Node*[data.size()];
    for (auto i = 0; i < data.size(); i++)
    {
        auto nodesIdx = i - 1;
        auto r = Find(root, data[i][0]);
        while (r == nullptr)
        {
            root = nodes[nodesIdx--];
            r = Find(root, data[i][0]);
        }

        fout.print_i32(r->balance);
        fout.print_char('\n');
        nodes[i] = root;
        root = r;
    }

    return 0;
}
