#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"
#include <stack>
#include <cmath>

#pragma comment(linker, "/STACK:100000000")

using namespace std;


struct Node
{
    int key;
    Node * left;
    Node * right;
    Node * parent;
    int height;

    Node(int _key, Node * _parent)
    {
        key = _key;
        parent = _parent;
        left = nullptr;
        right = nullptr;
        height = 0;
    }

    ~Node()
    {
        if (this->parent != nullptr)
        {
            if (this->parent->left == this)
                this->parent->left = nullptr;
            else
                this->parent->right = nullptr;
        }

        delete left;
        delete right;
    }
};


void FillTree(Node * &_root, int _line, vector<vector<int>> &_data, Node * _parent = nullptr)
{
    _root = new Node(_data[_line][0], _parent);

    if (_data[_line][1] != 0)
        FillTree(_root->left, _data[_line][1] - 1, _data, _root);

    if (_data[_line][2] != 0)
        FillTree(_root->right, _data[_line][2] - 1, _data, _root);
}


void CalcHeight(Node * _root)
{
    if (_root == nullptr)
        return;
    if (_root->left != nullptr)
        CalcHeight(_root->left);
    if (_root->right != nullptr)
        CalcHeight(_root->right);

    auto hl = _root->left == nullptr ? 0 : _root->left->height;
    auto hr = _root->right == nullptr ? 0 : _root->right->height;

    _root->height = max(hl, hr) + 1;
}


void RotateLeft(Node * &_root)
{
    Node * newRoot = _root->right;
    _root->right = newRoot->left;
    newRoot->left = _root;
    _root = newRoot;

    CalcHeight(_root);
}


void RotateRight(Node * &_root)
{
    Node * newRoot = _root->left;
    _root->left = newRoot->right;
    newRoot->right = _root;
    _root = newRoot;

    CalcHeight(_root);
}


void BigRotateLeft(Node * &_root)
{
    RotateRight(_root->right);
    RotateLeft(_root);
}


void BigRotateRight(Node * &_root)
{
    RotateLeft(_root->left);
    RotateRight(_root);
}


void Print(Node * _root, int _size)
{
    vector<int> sizes(_root->height);
    for (size_t i = 0; i < sizes.size(); i++)
    {
        sizes[i] = pow(2, i);
    }

    vector<vector<Node*>*>rows(_root->height);
    for (size_t i = 0; i < rows.size(); i++)
    {
        rows[i] = new vector<Node*>(pow(2, i), nullptr);
    }

    rows[0]->operator[](0) = _root;

    for (size_t i = 0; i < rows.size() - 1; i++)
    {
        for (size_t j = 0; j < rows[i]->size(); j++)
        {
            auto & rowRef = *rows[i + 1]; // ссылка на вектор указателей на Node, которые находятся в i+1 строке

            if (rows[i]->operator[](j) != nullptr)
            {
                rowRef[2 * j] = rows[i]->operator[](j)->left;
                rowRef[2 * j + 1] = rows[i]->operator[](j)->right;
            }
        }
    }

    openedu_out fout;

    fout.print_i32(_size);
    fout.print_char('\n');

    auto line = 2;
    for (size_t i = 0; i < rows.size(); i++)
    {
        auto & rowRef = *rows[i]; // ссылка на вектор - строку

        for (size_t i = 0; i < rowRef.size(); i++)
        {
            if (rowRef[i] == nullptr)
                continue;

            fout.print_i32(rowRef[i]->key);
            fout.print_char(' ');

            (rowRef[i]->left == nullptr) ? fout.print_i32(0) : fout.print_i32(line++);
            fout.print_char(' ');
            (rowRef[i]->right == nullptr) ? fout.print_i32(0) : fout.print_i32(line++);

            fout.print_char('\n');
        }
    }
}


int main()
{
    openedu_in fin;
    int size = fin.next_i32(-1);
    Node * root = nullptr;

    vector<vector<int>> data(size, vector<int>(3));

    if (size > 0)
    {
        for (auto i = 0; i < size; i++)
        {
            for (auto j = 0; j < 3; j++)
            {
                data[i][j] = fin.next_i32(-1);
            }
        }

        FillTree(root, 0, data);
    }


    CalcHeight(root);

    auto hl = root->right->left == nullptr ? -1 : root->right->left->height - 1;
    auto hr = root->right->right == nullptr ? -1 : root->right->right->height - 1;

    auto balance = hr - hl;

    if (balance == -1)
        BigRotateLeft(root);
    else
        RotateLeft(root);

    Print(root, size);


    return 0;
}
