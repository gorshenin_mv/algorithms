
#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"
#include <stack>
#include <cmath>

//#pragma comment(linker, "/STACK:100000000")

using namespace std;

struct InData
{
    string cmd;
    int val;
};

struct node // структура для представления узлов дерева
{
    int key;
    int height;
    node* left;
    node* right;
    node(int k) { key = k; left = right = 0; height = 1; }
};

int height(node* p)
{
    return p?p->height:0;
}

int bfactor(node* p)
{
    return height(p->right)-height(p->left);
}

void fixheight(node* p)
{
    if (!p)
        return;

    int hl = height(p->left);
    int hr = height(p->right);
    p->height = (hl>hr?hl:hr)+1;
}

node* rotateright(node* p) // правый поворот вокруг p
{
    node* q = p->left;
    p->left = q->right;
    q->right = p;
    fixheight(p);
    fixheight(q);
    return q;
}

node* rotateleft(node* q) // левый поворот вокруг q
{
    node* p = q->right;
    q->right = p->left;
    p->left = q;
    fixheight(q);
    fixheight(p);
    return p;
}

node* balance(node* p) // балансировка узла p
{
    fixheight(p);
    if( bfactor(p)==2 )
    {
        if( bfactor(p->right) < 0 )
            p->right = rotateright(p->right);
        return rotateleft(p);
    }
    if( bfactor(p)==-2 )
    {
        if( bfactor(p->left) > 0  )
            p->left = rotateleft(p->left);
        return rotateright(p);
    }
    return p; // балансировка не нужна
}

node* insert(node* p, int k) // вставка ключа k в дерево с корнем p
{
    if( !p ) return new node(k);
    if( k < p->key )
        p->left = insert(p->left,k);
    else if (k > p->key)
        p->right = insert(p->right,k);
    else return p;

    return balance(p);
}

node* findmin(node* p) // поиск узла с минимальным ключом в дереве p
{
    return p->right?findmin(p->right):p;
}

node* removemin(node* p) // удаление узла с минимальным ключом из дерева p
{
    if( p->right==0 )
        return p->left;
    p->right = removemin(p->right);
    return balance(p);
}

node* remove(node* p, int k) // удаление ключа k из дерева p
{
    if( !p ) return 0;
    if( k < p->key )
        p->left = remove(p->left,k);
    else if( k > p->key )
        p->right = remove(p->right,k);
    else //  k == p->key
    {
        node* q = p->left;
        node* r = p->right;
        delete p;
        if( !q ) return r;
        node* min = findmin(q);
        min->left = removemin(q);
        min->right = r;
        return balance(min);
    }
    return balance(p);
}


bool find(node* _root, int _key)
{
    if (_root == nullptr)
        return false;

    if (_root->key > _key)
    {
        return find(_root->left, _key);
    }
    else if (_root->key < _key)
    {
        return find(_root->right, _key);
    }
    else
        return true;
}

int main()
{
    openedu_in fin;
    openedu_out fout;
    int size = fin.next_i32(-1);
    node * root = nullptr;

    InData dat;

    if (size > 0)
    {
        for (auto i = 0; i < size; i++)
        {
            dat.cmd = fin.next();
            dat.val = fin.next_i32(0);

            if (dat.cmd == "A")
            {
                root = insert(root, dat.val);
                root != nullptr ? fout.print_i32(bfactor(root)) : fout.print_i32(0);
                fout.print_char('\n');
            }
            else if (dat.cmd == "D")
            {
                root = remove(root, dat.val);
                root != nullptr ? fout.print_i32(bfactor(root)) : fout.print_i32(0);
                fout.print_char('\n');
            }
            else
            {
                string res = find(root, dat.val) ? "Y" : "N";
                fout.print(res);
                fout.print_char('\n');
            }
        }
    }

    return 0;
}
