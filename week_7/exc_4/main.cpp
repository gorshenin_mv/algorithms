#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"
#include <stack>
#include <cmath>

#pragma comment(linker, "/STACK:100000000")

using namespace std;


struct Node
{
    int key;
    Node * left;
    Node * right;
    int diff;

    Node(int _key)
    {
        key = _key;
        left = nullptr;
        right = nullptr;
        diff = 0;
    }
};


void FillTree(Node * &_root, int _line, vector<vector<int>> &_data)
{
    _root = new Node(_data[_line][0]);

    if (_data[_line][1] != 0)
        FillTree(_root->left, _data[_line][1] - 1, _data);

    if (_data[_line][2] != 0)
        FillTree(_root->right, _data[_line][2] - 1, _data);
}


int CalcHeight(Node * _root)
{
    int h1 = 0, h2 = 0;

    if (_root == nullptr)
        return 0;
    if (_root->left != nullptr)
        h1 = CalcHeight(_root->left);
    if (_root->right != nullptr)
        h2 = CalcHeight(_root->right);

    _root->diff = h2 - h1;

    return max(h1, h2) + 1;
}


int CalcBalance(Node * _root)
{
    if (_root->right == nullptr && _root->left == nullptr)
        return 0;

    else if (_root->right == nullptr && _root->left != nullptr && _root->left->left == nullptr && _root->left->right == nullptr)
        return -1;

    else if (_root->left == nullptr && _root->right != nullptr && _root->right->left == nullptr && _root->right->right == nullptr)
        return 1;

    _root->left->diff = CalcBalance(_root->left);
    _root->right->diff = CalcBalance(_root->right);

    return _root->right->diff - _root->left->diff;
}


int CalcNodeBalance(Node * _root)
{
    if (_root->right == nullptr && _root->left == nullptr)
        return 0;

    auto lb = _root->left == nullptr ? -1 : _root->left->diff;
    auto rb = _root->right == nullptr ? -1 : _root->right->diff;

    return rb - lb;
}


Node * RotateLeft(Node * _root)
{
    auto newRoot = _root->right;
    _root->right = newRoot->left;
    newRoot->left = _root;
    return newRoot;
}


Node * RotateRight(Node * _root)
{
    auto newRoot = _root->left;
    _root->left = newRoot->right;
    newRoot->right = _root;
    return newRoot;
}


Node * BigRotateLeft(Node * _root)
{
    _root->right = RotateRight(_root->right);
    _root = RotateLeft(_root);
    return _root;
}


Node * BigRotateRight(Node * _root)
{
   _root->left = RotateLeft(_root->left);
   _root = RotateRight(_root);
   return _root;
}

Node * Balance(Node * _root)
{
    if (_root == nullptr)
        return nullptr;

    CalcHeight(_root);

    if (_root->diff == 2)
    {
        auto b = _root->right->diff;

        if (b == -1)
        {
            auto c = _root->right->left->diff;

            _root = BigRotateLeft(_root);

            if (c == 0)
            {
                _root->diff = 0;
                _root->left->diff = 0;
                _root->right->diff = 0;
            }
            else if (c == 1)
            {
                _root->diff = 0;
                _root->left->diff = -1;
                _root->right->diff = 0;
            }
            else
            {
                _root->diff = 0;
                _root->left->diff = 1;
                _root->right->diff = 0;
            }

        }

        else
        {
            _root = RotateLeft(_root);

            if (b == 0)
            {
                _root->diff = -1;
                _root->left->diff = 1;
            }
            else
            {
                _root->diff = 0;
                _root->left->diff = 0;
            }
        }
    }
    else if (_root->diff == -2)
    {
        auto b = _root->left->diff;

        if (b == 1)
        {
            auto c = _root->left->right->diff;

            _root = BigRotateRight(_root);

            if (c == 0)
            {
                _root->diff = 0;
                _root->left->diff = 0;
                _root->right->diff = 0;
            }
            else if (c == 1)
            {
                _root->diff = 0;
                _root->left->diff = -1;
                _root->right->diff = 0;
            }
            else
            {
                _root->diff = 0;
                _root->left->diff = 0;
                _root->right->diff = 1;
            }

        }

        else
        {
            _root = RotateRight(_root);

            if (b == 0)
            {
                _root->diff = 1;
                _root->right->diff = -1;
            }
            else
            {
                _root->diff = 0;
                _root->left->diff = 0;
            }
        }
    }

    return _root;
}


Node * Insert(Node * _root, int _key, bool &_forceExit)
{
    _forceExit = false;
    if (_root == nullptr)
    {
        _root = new Node(_key);
        return _root;
    }

    if (_root->key > _key)
    {
        _root->left = Insert(_root->left, _key, _forceExit);

        if (_forceExit)
            return _root;

        _root->diff--;

        if (_root->diff == 0)
        {
            _forceExit = true;
            return _root;
        }
        else if (_root->diff == 1 || _root->diff == -1)
            return _root;
        else
        {
            //_root->diff = CalcBalance(_root);
            _root = Balance(_root);
            if (_root->diff == 0)
                _forceExit = true;
            return _root;
        }
    }
    else if (_root->key < _key)
    {
        _root->right = Insert(_root->right, _key, _forceExit);

        if (_forceExit)
        {
            return _root;
        }

        _root->diff++;

        if (_root->diff == 0)
        {
            _forceExit = true;
            return _root;
        }
        else if (_root->diff == 1 || _root->diff == -1)
            return _root;
        else
        {
            //_root->diff = CalcBalance(_root);
            _root = Balance(_root);
            if (_root->diff == 0)
                _forceExit = true;
            return _root;
        }
    }

    else
    {
        _forceExit = true;
        return _root;
    }

}


Node * FindMin(Node * _root)
{
    if(_root->right == nullptr)
        return _root;
    else
        return FindMin(_root->right);
}


Node * RemoveMin(Node * _root)
{
    if (_root->right == nullptr)
        return _root->left;
    else
    {
        _root->right = RemoveMin(_root->right);
        return Balance(_root);
    }
}


Node * DelNode(Node * _root, int _key)
{
    if (_root->key > _key)
    {
        _root->left = DelNode(_root->left, _key);
    }
    else if (_root->key < _key)
    {
        _root->right = DelNode(_root->right, _key);
    }
    else
    {
        Node * tmp = _root->left;
        delete _root;
        return tmp;
    }

    return Balance(_root);
}


Node * Remove(Node * _root, int _key, bool & _forceExit)
{
    if (_root == nullptr)
    {
        _forceExit = true;
        return nullptr;
    }


    if (_root->key > _key)
    {
        _root->left = Remove(_root->left, _key, _forceExit);

        return Balance(_root);

        if (_forceExit)
            return _root;

        _root->diff++;

        if (_root->diff == 0)
            return _root;

        else if (_root->diff == 1 || _root->diff == -1)
        {
            _forceExit = true;
            return _root;
        }

        else
        {
            _root = Balance(_root);
            if (_root->diff == 1 || _root->diff == -1)
                _forceExit = true;
            return _root;
        }
    }
    else if (_root->key < _key)
    {
        _root->right = Remove(_root->right, _key, _forceExit);

        return Balance(_root);

        if (_forceExit)
            return _root;

        _root->diff--;

        if (_root->diff == 0)
            return _root;

        else if (_root->diff == 1 || _root->diff == -1)
        {
            _forceExit = true;
            return _root;
        }

        else
        {
            _root = Balance(_root);
            if (_root->diff == 1 || _root->diff == -1)
                _forceExit = true;
            return _root;
        }

    }
    else
    {
        auto l = _root->left;
        auto r = _root->right;

        if (l == r)
        {
            delete _root;
            return nullptr;
        }
        else if (l == nullptr)
        {
            return r;
        }
        else
        {
            Node * min = FindMin(l);
            _root->key = min->key;
            _root->left = DelNode(l, _root->key);
            //_root->left = Balance(_root->left);
            return Balance(_root);

//            Node * min = FindMin(l);
//            min->left = RemoveMin(l);
//            min->right = r;
//            return Balance(min);
        }
    }
}


void Print(Node * _root, int _size)
{
    if (_root == nullptr)
    {
        openedu_out fout;
        fout.print_i32(0);
        return;
    }

    auto height = CalcHeight(_root);

    vector<vector<Node*>*>rows(height);
    for (size_t i = 0; i < rows.size(); i++)
    {
        rows[i] = new vector<Node*>(pow(2, i), nullptr);
    }

    rows[0]->operator[](0) = _root;

    for (size_t i = 0; i < rows.size() - 1; i++)
    {
        for (size_t j = 0; j < rows[i]->size(); j++)
        {
            auto & rowRef = *rows[i + 1]; // ссылка на вектор указателей на Node, которые находятся в i+1 строке

            if (rows[i]->operator[](j) != nullptr)
            {
                rowRef[2 * j] = rows[i]->operator[](j)->left;
                rowRef[2 * j + 1] = rows[i]->operator[](j)->right;
            }
        }
    }

    openedu_out fout;

    fout.print_i32(_size);
    fout.print_char('\n');

    auto line = 2;
    for (size_t i = 0; i < rows.size(); i++)
    {
        auto & rowRef = *rows[i]; // ссылка на вектор - строку

        for (size_t i = 0; i < rowRef.size(); i++)
        {
            if (rowRef[i] == nullptr)
                continue;

            fout.print_i32(rowRef[i]->key);
            fout.print_char(' ');

            (rowRef[i]->left == nullptr) ? fout.print_i32(0) : fout.print_i32(line++);
            fout.print_char(' ');
            (rowRef[i]->right == nullptr) ? fout.print_i32(0) : fout.print_i32(line++);

            fout.print_char('\n');
        }
    }
}


int main()
{
    openedu_in fin;
    int size = fin.next_i32(-1);
    Node * root = nullptr;

    vector<vector<int>> data(size, vector<int>(3));

    if (size > 0)
    {
        for (auto i = 0; i < size; i++)
        {
            for (auto j = 0; j < 3; j++)
            {
                data[i][j] = fin.next_i32(-1);
            }
        }

        FillTree(root, 0, data);
        CalcHeight(root);
    }

    auto key = fin.next_i32(-1);

    bool forceExit = false;
    auto newNode = Remove(root, key, forceExit);

    Print(newNode, size - 1);
    return 0;
}
