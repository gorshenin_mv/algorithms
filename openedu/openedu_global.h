#ifndef OPENEDU_GLOBAL_H
#define OPENEDU_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(OPENEDU_LIBRARY)
#  define OPENEDUSHARED_EXPORT Q_DECL_EXPORT
#else
#  define OPENEDUSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // OPENEDU_GLOBAL_H
