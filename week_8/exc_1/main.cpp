#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"
#include <stack>
#include <cmath>

using namespace std;

class BTree
{
private:
    struct node // структура для представления узлов дерева
    {
        long long key;
        int height;
        node* left;
        node* right;
        node(long long k) { key = k; left = right = 0; height = 1; }
    };

    node * root;


    int height(node* p)
    {
        return p?p->height:0;
    }


    int bfactor(node* p)
    {
        return height(p->right)-height(p->left);
    }


    void fixheight(node* p)
    {
        if (!p)
            return;

        int hl = height(p->left);
        int hr = height(p->right);
        p->height = (hl>hr?hl:hr)+1;
    }

    node* rotateright(node* p) // правый поворот вокруг p
    {
        node* q = p->left;
        p->left = q->right;
        q->right = p;
        fixheight(p);
        fixheight(q);
        return q;
    }


    node* rotateleft(node* q) // левый поворот вокруг q
    {
        node* p = q->right;
        q->right = p->left;
        p->left = q;
        fixheight(q);
        fixheight(p);
        return p;
    }


    node* balance(node* p) // балансировка узла p
    {
        fixheight(p);
        if( bfactor(p)==2 )
        {
            if( bfactor(p->right) < 0 )
                p->right = rotateright(p->right);
            return rotateleft(p);
        }
        if( bfactor(p)==-2 )
        {
            if( bfactor(p->left) > 0  )
                p->left = rotateleft(p->left);
            return rotateright(p);
        }
        return p; // балансировка не нужна
    }


    node* insert(node* p, long long k) // вставка ключа k в дерево с корнем p
    {
        if( !p ) return new node(k);
        if( k < p->key )
            p->left = insert(p->left,k);
        else if (k > p->key)
            p->right = insert(p->right,k);
        else return p;

        return balance(p);
    }


    node* remove(node* p, long long k) // удаление ключа k из дерева p
    {
        if( !p ) return nullptr;
        if( k < p->key )
            p->left = remove(p->left,k);
        else if( k > p->key )
            p->right = remove(p->right,k);
        else //  k == p->key
        {
            node* q = p->left;
            node* r = p->right;
            delete p;
            if( !q ) return r;
            node* min = findmin(q);
            min->left = removemin(q);
            min->right = r;
            return balance(min);
        }
        return balance(p);
    }


    bool find(node* _root, long long _key)
    {
        if (_root == nullptr)
            return false;

        if (_root->key > _key)
        {
            return find(_root->left, _key);
        }
        else if (_root->key < _key)
        {
            return find(_root->right, _key);
        }
        else
            return true;
    }


    node* findmin(node* p) // поиск узла с минимальным ключом в дереве p
    {
        return p->right?findmin(p->right):p;
    }


    node* removemin(node* p) // удаление узла с минимальным ключом из дерева p
    {
        if( p->right==0 )
            return p->left;
        p->right = removemin(p->right);
        return balance(p);
    }


public:
    BTree() {root = nullptr;}


    void Insert(long long _key)
    {
        root = insert(root, _key);
    }


    void Remove(long long _key)
    {
        root = remove(root, _key);
    }


    bool Find(long long _key)
    {
        return find(root, _key);
    }
};





int main()
{
    openedu_in fin;
    openedu_out fout;
    int size = fin.next_i32(-1);
    int sNum = 997;
    int buckets = sNum * 2;

    vector<BTree*> trees(buckets);

    for (auto i = 0; i < size; i++)
    {
        auto c = fin.next();
        auto d = fin.next_i64(-1);

        auto bNum = d < 0 ? abs(d) % sNum : d % sNum + sNum;

        if (c == "A")
        {
            if (trees[bNum] == nullptr)
                trees[bNum] = new BTree();

            BTree * b = trees[bNum];
            b->Insert(d);
        }
        else if (c == "D")
        {
            if (trees[bNum] == nullptr)
                continue;

            BTree * b = trees[bNum];
            b->Remove(d);
        }
        else
        {
            if (trees[bNum] != nullptr)
            {
                BTree * b = trees[bNum];
                b->Find(d) ? fout.print_char('Y') : fout.print_char('N');
                fout.print_char('\n');
            }
            else
            {
                fout.print_char('N');
                fout.print_char('\n');
            }
        }
    }

    return 0;
}
