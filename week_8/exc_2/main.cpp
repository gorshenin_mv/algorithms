#include <iostream>
#include <vector>
#include <list>
#include "openedu.hpp"

using namespace std;

struct Item
{
    unsigned long long hashcode;
    string key;
    string value;

    Item * prevKey;
    Item * nextKey;
};


unsigned long long Hash(const string &_key)
{
    const int p = 53;
    unsigned long long hash = 0, pPow = 1;
    for (size_t i = 0; i < _key.size(); ++i)
    {
        hash += (_key[i] - 'a' + 1) * pPow;
        pPow *= p;
    }

    return hash;
}


list<Item>::iterator Find(string &_key, vector<list<Item>> &_table, int _b, bool  &_ok)
{
    auto h = Hash(_key);
    auto bIdx = h % _b;
    list<Item> & l = _table[bIdx];

    for(auto i = l.begin(); i != l.end(); i++)
    {
        Item & item = *i;

        if (item.hashcode == h && item.key == _key)
        {
            _ok = true;
            return i;
        }
    }

    _ok = false;
    return l.end();
}


//list<Item>::iterator Find(string &_key, vector<list<Item>> &_table, int _b, bool  &_ok)
//{
//    auto h = Hash(_key);
//    auto bIdx = h % _b;
//    list<Item> & l = _table[bIdx];

//    for(auto i = l.begin(); i != l.end(); i++)
//    {
//        Item & item = *i;

//        if (item.hashcode == h && item.key == _key)
//        {
//            _ok = true;
//            return i;
//        }
//    }

//    _ok = false;
//    return l.end();
//}

int main()
{
    const int buckets = 999983;
    vector<list<Item>> table(buckets);
    openedu_in fin;
    openedu_out fout;

    size_t size = fin.next_i32(-1);
    Item * prev = nullptr;

    for (size_t i = 0; i < size; i++)
    {
        auto c = fin.next();
        auto k = fin.next();

        auto h = Hash(k);
        auto bIdx = h % buckets;
        list<Item> &l = table[bIdx];

        if (c == "put")
        {
            auto v = fin.next();
            auto ok = false;
            auto it = Find(k, table, buckets, ok);

            if (ok)
            {
                Item & item = *it;
                item.value = v;
            }
            else
            {
                Item * prevPrev = prev;
                l.push_front(Item{h, k, v, prev, nullptr});
                prev = &l.front();

                if (prevPrev != nullptr)
                    prevPrev->nextKey = prev;

            }
        }
        else if (c == "get")
        {
            auto ok = false;
            auto it = Find(k, table, buckets, ok);

            if (ok)
                fout.println(it->value);
            else
                fout.print("<none>\n");
        }
        else if (c == "prev")
        {
            auto ok = false;
            auto it = Find(k, table, buckets, ok);

            if (ok && it->prevKey != nullptr)
            {
                fout.println(it->prevKey->value);
            }
            else
                fout.print("<none>\n");

        }
        else if (c == "next")
        {
            auto ok = false;
            auto it = Find(k, table, buckets, ok);

            if (ok && it->nextKey != nullptr)
            {
                fout.println(it->nextKey->value);
            }
            else
                fout.print("<none>\n");
        }
        else
        {
            auto ok = false;
            auto it = Find(k, table, buckets, ok);

            if(ok)
            {
                Item & item = *it;

                if (&item == prev)
                    prev = item.prevKey;


                if (item.prevKey != nullptr)
                    item.prevKey->nextKey = item.nextKey;

                if (item.nextKey != nullptr)
                    item.nextKey->prevKey = item.prevKey;

                l.erase(it);
            }
        }
    }


    return 0;
}
