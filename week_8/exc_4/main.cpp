#include <iostream>
#include <cstring>
#include "openedu.hpp"

using namespace std;

int Hash(string & _str)
{
    int rv = 0;

    for (size_t i = 0; i < _str.size(); i++)
    {
        rv = 101 * rv + _str[i];
    }

    return rv;
}

string GetStr()
{
    string b = "cd";

    for (auto i = 0; i < 12; i++)
    {
        string tmp = "";

        for (auto j = 0; j < b.size(); j++)
        {
            if (b[j] == 'c')
                tmp += "d";
            else
                tmp += "c";

        }

        b += tmp;
    }

    return b;
}


void WrStrs(size_t _lvl, const char ** _ss, openedu_out &_fout, short * _ssIdxArr, int & _c, const int _m, const char * _suf)
{
    if (_lvl < 15)
    {
        for (short ssIdx = 0; ssIdx < 2; ++ssIdx)
        {
            _ssIdxArr[_lvl] = ssIdx;
            WrStrs(_lvl + 1, _ss, _fout, _ssIdxArr, _c, _m, _suf);

            if (_c >= _m)
                return;
        }

    }
    else
    {
        for (short ssIdx = 0; ssIdx < 2; ++ssIdx)
        {
            if (_c >= _m)
                return;

            _ssIdxArr[_lvl] = ssIdx;

            for (short ssIdxArrIdx = 0; ssIdxArrIdx < 16; ++ssIdxArrIdx)
            {
                _fout.print(_ss[_ssIdxArr[ssIdxArrIdx]]);
                //_fout.print_i32(_ssIdxArr[ssIdxArrIdx]);
            }

            _fout.print(_suf);
            _fout.print_char('\n');
            ++_c;
        }
    }
}


int main()
{
    openedu_in fin;
    size_t m = fin.next_i32(-1);

    openedu_out fout;

    const char * ss[] = {"cddcdccddccdcddcdccdcddccddcdccddccdcddccddcdccdcddcdccddccdcddcdccdcddccddcdccdcddcdccddccdcddccddcdccddccdcddcdccdcddccddcdccd", "dccdcddccddcdccdcddcdccddccdcddccddcdccddccdcddcdccdcddccddcdccdcddcdccddccdcddcdccdcddccddcdccddccdcddccddcdccdcddcdccddccdcddc"};
    const char * suf = "zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz";

    short lvl = 0;
    short ssIdxArr[16];
    int c = 0;

    WrStrs(lvl, ss, fout, ssIdxArr, c, m, suf);

    return 0;
}
