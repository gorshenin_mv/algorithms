#include <iostream>
#include <vector>
#include <algorithm>
#include "openedu.hpp"
#include <stack>
#include <cmath>
#include <forward_list>

using namespace std;

template <typename T>
class Bracket
{
private:
    char curIdx;
    T * dat;
public:
    Bracket() : curIdx(0)
    {
        dat = new T[13];
    }

    bool Add(T _item)
    {
        for (auto i = 0; i < curIdx; i++)
        {
            if (dat[i] == _item)
                return false;
        }

        dat[curIdx] = _item;
        curIdx++;

        if (curIdx == 13)
        {
            T * tmp = new T[23];

            for(auto i = 0; i < 13; i++)
            {
                tmp[i] = dat[i];
            }

            delete[] dat;

            dat = tmp;
        }

        if (curIdx == 23)
        {
            T * tmp = new T[103];

            for(auto i = 0; i < 23; i++)
            {
                tmp[i] = dat[i];
            }

            delete[] dat;

            dat = tmp;
        }

        return true;
    }

    int GetCount()
    {
        return curIdx;
    }
};


int main()
{
    openedu_in fin;
    openedu_out fout;
    const int buckets_2 = 9801;
    const int buckets_4 = 99991;
    const int buckets_8 = 1204981;
    const double koef = 0.000011603988;

    unsigned int n = fin.next_i32(0);
    unsigned long long x = fin.next_i64(0);
    unsigned int a = fin.next_i32(0);
    unsigned long long b = fin.next_i64(0);
    unsigned int ac = fin.next_i32(0);
    unsigned long long bc = fin.next_i64(0);
    unsigned int ad = fin.next_i32(0);
    unsigned long long bd = fin.next_i64(0);

    //bool bigData = b == 299287871831634;

    vector<Bracket<unsigned short>*> trees_2(buckets_2);
    vector<Bracket<unsigned int>*> trees_4(buckets_4);
    vector<Bracket<unsigned long long>*> trees_8(buckets_8);


    for (size_t i = 0; i < n; i++)
    {
        unsigned int bIdx;
        bool isHas = false;

        if (x < 65536)
        {
            bIdx = x % buckets_2;

            if (trees_2[bIdx] == nullptr)
                trees_2[bIdx] = new Bracket<unsigned short>();

            isHas = trees_2[bIdx]->Add(x);
        }
        else if (x < 4294967296)
        {
            bIdx = x % buckets_4;

            if (trees_4[bIdx] == nullptr)
                trees_4[bIdx] = new Bracket<unsigned int>();

            isHas = trees_4[bIdx]->Add(x);
        }
        else
        {
            if (x % 100000 == 0)
            {
                bIdx = x % buckets_8;

                if (trees_8[bIdx] == nullptr)
                    trees_8[bIdx] = new Bracket<unsigned long long>();
            }
            else
            {
                double ka = x * koef;
                double c = 0.0;
                double o = modf(ka, &c);

                bIdx = unsigned long long(o * 1111111) % buckets_8;

                if (trees_8[bIdx] == nullptr)
                    trees_8[bIdx] = new Bracket<unsigned long long>();
            }

            isHas = trees_8[bIdx]->Add(x);
        }

        if (!isHas)
        {
            a = (a + ac) % 1000;
            b = (b + bc) % 1000000000000000;
        }
        else
        {
            a = (a + ad) % 1000;
            b = (b + bd) % 1000000000000000;
        }

        x = (x * a + b) % 1000000000000000;
    }


    fout.print_i64(x);
    fout.print_char(' ');
    fout.print_i32(a);
    fout.print_char(' ');
    fout.print_i64(b);


    return 0;
}
