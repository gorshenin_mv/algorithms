#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    int inDat[2];

    ifstream fin;
    fin.open("input.txt");

    short i = 0;
    while(fin >> inDat[i] && i < 2)
    {
       i++;
    }
    fin.close();

    ofstream fout;
    fout.open("output.txt");

    fout << inDat[0] + static_cast<long long>(inDat[1]) * inDat[1];
    fout.close();

    return 0;
}
