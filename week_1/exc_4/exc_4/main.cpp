#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

struct Man
{
    unsigned short idx;
    double income;

    bool operator < (Man &_m)
    {
        return (*this).income < _m.income;
    }
};

int main()
{
    std::ifstream fin("input.txt");
    unsigned short size = 0;

    if (!fin.is_open())
    {
        std::cerr << "Open input file Error!";
        return 1;
    }

    fin >> size;
    std::vector<Man> mans(size);

    for (int i = 0; i < size; i++)
    {
        mans[i].idx = i+1;
        fin >> mans[i].income;
    }

    fin.close();

    std::sort(mans.begin(), mans.end());

    std::ofstream fout("output.txt");

    if (!fout.is_open())
    {
        std::cerr << "Open output file Error!";
        return 1;
    }

    fout << mans[0].idx << ' ' << mans[size / 2].idx << ' ' << mans[size - 1].idx << std::flush;

    fout.close();

    return 0;
}
