#include <iostream>
#include <fstream>
#include <vector>

//using namespace std;

void sort(int * _arr, int _size, std::vector<int> &_idxes)
{
    if (_size > 0)
        _idxes.insert(_idxes.end(), 1);

    for (int i = 1; i < _size; i++)
    {
        int idx = i - 1;

        while(idx >= 0 && _arr[idx] > _arr[idx+1])
        {
            std::swap(_arr[idx], _arr[idx+1]);
            idx--;
        }

        _idxes.insert(_idxes.end(), idx+2);
    }
}

int main()
{        
    int size = 0;
    std::ifstream fin;

    fin.open("input.txt");
    fin >> size;

    int * inDat = new int[size];
    int idx = 0;

    while (idx < size && fin >> inDat[idx])
        idx++;

    fin.close();

    std::vector<int> idxes;
    sort(inDat, size, idxes);

    std::ofstream fout;
    fout.open("output.txt");

    for(int i = 0; i < idxes.size(); i++)
    {
        fout << idxes[i] << ' ';
    }

    fout << '\n';

    for(int i = 0; i < size; i++)
    {
        fout << inDat[i] << ' ';
    }

    fout << std::flush;
    fout.close();

    return 0;
}
