#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>

enum class Status : unsigned char {NotChanged, GoodPos};

struct Elem
{
    unsigned short oldIdx;
    unsigned short newIdx;
    int val;

    bool operator <(Elem &_elem)
    {
        return (*this).val < _elem.val;
    }
};

int main()
{
    // Открытие файла для чтения входных данных
    std::ifstream fin("input.txt");
    if (!fin.is_open())
        return EXIT_FAILURE;

    // Определение размера массива с данными
    int size = 0;
    fin >> size;

    // Создание массивов для хранения данных
    Status * sta = new Status[size];
    std::vector<Elem> base(size);
    std::vector<Elem> sorting(size);

    // Инициализация массивов исходными данными
    for (int i = 0; i < size; i++)
    {
        sta[i] = Status::NotChanged;
        fin >> sorting[i].val;
        base[i].val = sorting[i].val;
        base[i].oldIdx = sorting[i].oldIdx = i;
    }
    fin.close();

    std::sort(sorting.begin(), sorting.end());

    // Задача: прописать для элементов базвого массива новые индексы
    // Задачу можно решить, используя старые индексы в отсортированном массиве
    // как индекс для базового массива и в поле newIdx прописать i, т.к.
    // i это новый индекс
    for (int i = 0; i < size; i++)
    {
        base[sorting[i].oldIdx].newIdx = i;
    }

    int x = 0;
    int y = 0;
    int wr_x = 0;
    int wr_y = 0;

    std::ofstream fout("output.txt");
    if (!fout.is_open())
        return EXIT_FAILURE;

    for (int i = 0; i < size; i++)
    {
        if (sta[i] != Status::NotChanged)
            continue;

        x = i;
        y = base[i].newIdx;

        while (x != y)
        {
            wr_x = x + 1;
            wr_y = y + 1;

            if(wr_x > wr_y)
                std::swap(wr_x, wr_y);

            fout << "Swap elements at indices " << wr_x << " and " << wr_y << ".\n";

            sta[y] = Status::GoodPos;
            y = base[y].newIdx;
        }
    }
    fout << "No more swaps needed.\n";

    for (int i = 0; i < size; i++)
    {
        fout << sorting[i].val << ' ';
    }
    fout << std::flush;
    fout.close();

    return 0;
}
