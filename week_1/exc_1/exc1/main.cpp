#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    int inDat[2];

    ifstream fin;
    fin.open("input.txt");

//    if (!fin.is_open())
//        std::cout << "Error!" << std::endl;

    short i = 0;
    while(fin >> inDat[i] && i < 2)
    {
       i++;
    }
    fin.close();

    ofstream fout;
    fout.open("output.txt");

    if(!fout.is_open())
        std::cout << "Error!" << std::endl;

    fout << inDat[0] + inDat[1];
    fout.close();

    return 0;
}
