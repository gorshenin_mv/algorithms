#include <iostream>
#include <fstream>
#include <cstring>

using namespace std;

// Создать счетчики.
// data-сортируемый массив, counters-массив для счетчиков, N-число элементов в data
template<class T>
void createCounters(T *data, long *counters, long N) {
    // i-й массив count расположен, начиная с адреса counters+256*i
    memset(counters, 0, 256*sizeof(T)*sizeof(long) );

    unsigned char *bp = (unsigned char*)data;
    unsigned char *dataEnd = (unsigned char*)(data + N);
    unsigned short i;

    while ( bp != dataEnd ) {
        // увеличиваем количество байт со значением *bp
        // i - текущий массив счетчиков
        for (i=0; i<sizeof(T);i++)
            counters[256*i + *bp++]++;
    }
}


// Функция radixPass принимает в качестве параметров
// номер байта Offset,
// число элементов N,
// исходный массив source,
// массив dest, куда будут записываться числа, отсортированные по байту Offset
// массив счетчиков count, соответствующий текущему проходу.

template<class T>
void radixPass (short Offset, long N, T *source, T *dest, long *count) {
    // временные переменные
    T *sp;
    long s, c, i, *cp;
    unsigned char *bp;


    // шаг 3
    s = 0; 	// временная переменная, хранящая сумму на данный момент
    cp = count;
    for (i = 256; i > 0; --i, ++cp) {
        c = *cp;
        *cp = s;
        s += c;
    }

    // шаг 4
    bp = (unsigned char *)source + Offset;
    sp = source;
    for (i = N; i > 0; --i, bp += sizeof(T) , ++sp) {
        cp = count + *bp;
        dest[*cp] = *sp;
        ++(*cp);
    }
}

// сортируется массив in из N элементов
// T - любой беззнаковый целый тип
template<class T>
void radixSort (T* &in, long N) {
    T *out = new T[N];
    long *counters = new long[sizeof(T)*256], *count;
    createCounters(in, counters, N);

    for (unsigned short i=0; i<sizeof(T); i++) {
        count = counters + 256*i;         // count - массив счетчиков для i-го разряда

        if ( count[0] == N ) continue;    // (*** см ниже)

        radixPass (i, N, in, out, count); // после каждого шага входной и
        swap(in, out);                    // выходной массивы меняются местами
    }
                          // по окончании проходов
    delete[] out;           // вся информация остается во входном массиве.
    delete[] counters;
}

int main()
{
    // Заполнение и инициализация входных массивов
    std::ifstream fin("input.txt");
    if (!fin.is_open())
        return EXIT_FAILURE;

    unsigned short size_a = 0, size_b = 0;
    fin >> size_a; fin >> size_b;

    auto inDat_a = new unsigned short[size_a];
    auto inDat_b = new unsigned short[size_b];

    for (unsigned short a_idx = 0; a_idx < size_a; a_idx++)
    {
        fin >> inDat_a[a_idx];
    }

    for (unsigned short b_idx = 0; b_idx < size_b; b_idx++)
    {
        fin >> inDat_b[b_idx];
    }

    unsigned int unsorted_size = static_cast<unsigned int>(size_a) * size_b;
    auto unsorted = new unsigned int[unsorted_size];

    unsigned int unsorted_idx = 0;
    for (unsigned short a_idx = 0; a_idx < size_a; a_idx ++)
    {
        for (unsigned short b_idx = 0; b_idx < size_b; b_idx ++)
        {
            unsorted[unsorted_idx] = inDat_a[a_idx] * inDat_b[b_idx];
            //std::cout << unsorted[unsorted_idx] << ' ';
            unsorted_idx++;
        }
    }

    delete[] inDat_a; delete[] inDat_b;
    radixSort(unsorted, unsorted_size);

    unsigned long long sum = 0;
    for (unsigned int unsorted_idx = 0; unsorted_idx < unsorted_size; unsorted_idx += 10)
    {

        sum += unsorted[unsorted_idx];
    }

    // Вывод результата
    std::ofstream fout("output.txt");
    if (!fout.is_open())
        return EXIT_FAILURE;

    fout << sum << std::flush;
    fout.close();

    return 0;
}
