﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace radixsort4
{
    class Program
    {

        static string GenerateResult(SortElem[] _array)
        {
            var sb = new StringBuilder();
            foreach (var item in _array)
            {
                sb.Append(item.index + 1);
                sb.Append(' ');
            }
            return sb.ToString();
        }

        static void WriteResult(string _result)
        {
            using (var writer = new StreamWriter(new FileStream("output.txt", FileMode.Create)))
            {
                writer.Write(_result);
            }
        }

        struct SortElem
        {
            public int index;
            public byte value;
        }

        static void Main(string[] args)
        {
            string line1;
            byte[] line;
            var sw = new Stopwatch();

            
            using (var reader = new StreamReader(new FileStream("input.txt", FileMode.Open)))
            {
                line1 = reader.ReadLine();
            }

            int[] cns = line1.Split(' ').Select(x => Convert.ToInt32(x)).ToArray();
            int offset = line1.Length + 2;
            line = File.ReadAllBytes("input.txt");
            int columns = cns[0];
            int rows = cns[1];
            byte[,] data = new byte[rows,columns];
            int newIndex;
            int iteration=0;
            bool isEven = false;

            SortElem[] elems1 = new SortElem[columns];
            SortElem[] elems2 = new SortElem[columns];
            for (int i = 0; i < columns; i++)
            {
                elems1[i].index = i;
            }

            //sw.Start();

            // Заполнение массива с данными
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    data[row, column] = line[offset++];
                }
                offset += 2;
            }
            ////////////////////////////////////


            // Сортировка подсчетом
            int[] counter = new int[26];

            for (int row = rows-1; row >-1; row--)
            {
                if (!isEven)
                {
                    for (int column = 0; column < columns; column++)
                    {
                        elems1[column].value = data[row, elems1[column].index];
                        counter[elems1[column].value - 97]++;
                    }

                    #region UpdateCounter
                    //UpdateCounter(counter);
                    counter[1] += counter[0];
                    counter[2] += counter[1];
                    counter[3] += counter[2];
                    counter[4] += counter[3];
                    counter[5] += counter[4];
                    counter[6] += counter[5];
                    counter[7] += counter[6];
                    counter[8] += counter[7];
                    counter[9] += counter[8];
                    counter[10] += counter[9];
                    counter[11] += counter[10];
                    counter[12] += counter[11];
                    counter[13] += counter[12];
                    counter[14] += counter[13];
                    counter[15] += counter[14];
                    counter[16] += counter[15];
                    counter[17] += counter[16];
                    counter[18] += counter[17];
                    counter[19] += counter[18];
                    counter[20] += counter[19];
                    counter[21] += counter[20];
                    counter[22] += counter[21];
                    counter[23] += counter[22];
                    counter[24] += counter[23];
                    counter[25] += counter[24];

                    #endregion

                    for (int i = columns - 1; i > -1; i--)
                    {
                        newIndex = counter[elems1[i].value - 97] - 1;
                        counter[elems1[i].value - 97]--;
                        elems2[newIndex] = elems1[i];

                    }
                    iteration++;
                    if (iteration == cns[2])
                    {
                        WriteResult(GenerateResult(elems2));
                    }
                    isEven = true;
                }
                else
                {
                    for (int column = 0; column < columns; column++)
                    {
                        elems2[column].value = data[row, elems2[column].index];
                        counter[elems2[column].value - 97]++;
                    }

                    #region UpdateCounter
                    //UpdateCounter(counter);
                    counter[1] += counter[0];
                    counter[2] += counter[1];
                    counter[3] += counter[2];
                    counter[4] += counter[3];
                    counter[5] += counter[4];
                    counter[6] += counter[5];
                    counter[7] += counter[6];
                    counter[8] += counter[7];
                    counter[9] += counter[8];
                    counter[10] += counter[9];
                    counter[11] += counter[10];
                    counter[12] += counter[11];
                    counter[13] += counter[12];
                    counter[14] += counter[13];
                    counter[15] += counter[14];
                    counter[16] += counter[15];
                    counter[17] += counter[16];
                    counter[18] += counter[17];
                    counter[19] += counter[18];
                    counter[20] += counter[19];
                    counter[21] += counter[20];
                    counter[22] += counter[21];
                    counter[23] += counter[22];
                    counter[24] += counter[23];
                    counter[25] += counter[24];

                    #endregion

                    for (int i = columns - 1; i > -1; i--)
                    {
                        newIndex = counter[elems2[i].value - 97] - 1;
                        counter[elems2[i].value - 97]--;
                        elems1[newIndex] = elems2[i];

                    }
                    iteration++;
                    if (iteration == cns[2])
                    {
                        WriteResult(GenerateResult(elems1));
                    }
                    isEven = false;
                }
                

                #region Zero
                counter[1] =
                counter[2] =
                counter[3] =
                counter[4] =
                counter[5] =
                counter[6] =
                counter[7] =
                counter[8] =
                counter[9] =
                counter[10] =
                counter[11] =
                counter[12] =
                counter[13] =
                counter[14] =
                counter[15] =
                counter[16] =
                counter[17] =
                counter[18] =
                counter[19] =
                counter[20] =
                counter[21] =
                counter[22] =
                counter[23] =
                counter[24] =
                counter[25] = 0;
#endregion
            }
        }
    }
}
